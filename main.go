package main

import (
	"flag"
	"fmt"
	"io"
	"mime"
	"net"
	"net/http"
	"strings"

	"github.com/golang/glog"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"golang.org/x/net/context"
	"google.golang.org/grpc"

	"github.com/philips/go-bindata-assetfs"
	pb "github.com/philips/grpc-gateway-example/echopb"
	"github.com/philips/grpc-gateway-example/pkg/ui/data/swagger"
	blah "github.com/utilitywarehouse/grpc-testing/blah"
)

var (
	echoEndpoint = flag.String("echo_endpoint", "localhost:9090", "endpoint of YourService")
)

func run() error {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	l, err := net.Listen("tcp", ":9090")
	if err != nil {
		panic(err)
	}

	gsrv := grpc.NewServer()
	server := blah.NewBlah()

	blah.RegisterBlahServiceServer(gsrv, server)
	mux := http.NewServeMux()
	opts := []grpc.DialOption{grpc.WithInsecure()}
	mux.HandleFunc("/swagger.json", func(w http.ResponseWriter, req *http.Request) {
		io.Copy(w, strings.NewReader(pb.Swagger))
	})

	gwmux := runtime.NewServeMux()
	err = blah.RegisterBlahServiceHandlerFromEndpoint(ctx, gwmux, *echoEndpoint, opts)
	if err != nil {
		fmt.Printf("serve: %v\n", err)
		return err
	}

	mux.Handle("/", gwmux)
	serveSwagger(mux)

	go func() {
		http.ListenAndServe(":8080", mux)
	}()

	// Spawn monitor to trigger a graceful shutdown once the context is done
	go func() {
		<-ctx.Done()
		gsrv.GracefulStop()
	}()

	if err = gsrv.Serve(l); err != nil {
		return err
	}

	_ = l.Close()
	return nil
}

func main() {
	flag.Parse()
	defer glog.Flush()

	if err := run(); err != nil {
		glog.Fatal(err)
	}
}

func serveSwagger(mux *http.ServeMux) {
	mime.AddExtensionType(".svg", "image/svg+xml")

	// Expose files in third_party/swagger-ui/ on <host>/swagger-ui
	fileServer := http.FileServer(&assetfs.AssetFS{
		Asset:    swagger.Asset,
		AssetDir: swagger.AssetDir,
		Prefix:   "third_party/swagger-ui",
	})
	prefix := "/swagger-ui/"
	mux.Handle(prefix, http.StripPrefix(prefix, fileServer))
}
