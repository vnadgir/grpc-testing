all: model-gen grpc-gw-gen swagger-gen

model-gen:
	mkdir -p ./blah
	cd ./blah
	protoc -I/usr/local/include -I. \
  	-I$(GOPATH)/src \
  	-I$(GOPATH)/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
  	--go_out=plugins=grpc:./blah \
    *.proto

stub-gen:
	mkdir -p ./blah
	cd ./blah
	protoc -I/usr/local/include -I. \
  	-I$(GOPATH)/src \
  	-I$(GOPATH)/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
  	--go_out=plugins=grpc:./blah \
    *.proto

grpc-gw-gen:
	mkdir -p ./blah
	cd ./blah
	protoc -I/usr/local/include -I. \
  	-I$(GOPATH)/src \
  	-I$(GOPATH)/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		--grpc-gateway_out=logtostderr=true:./blah \
    *.proto

swagger-gen:
	mkdir -p ./blah
	cd ./blah
	protoc -I/usr/local/include -I. \
  	-I$(GOPATH)/src \
  	-I$(GOPATH)/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		--swagger_out=logtostderr=true:./blah \
    *.proto
