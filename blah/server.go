package blah

import (
	"bufio"
	"context"
	"fmt"

	"bytes"

	"github.com/clbanning/mxj"
	"github.com/gogo/protobuf/proto"
	"github.com/golang/protobuf/jsonpb"
	"github.com/golang/protobuf/ptypes/any"
	_struct "github.com/golang/protobuf/ptypes/struct"
)

type blahtter struct{}

func NewBlah() BlahServiceServer {
	return blahtter{}
}

var m = jsonpb.Marshaler{}

func (b blahtter) EchoBlahAny(c context.Context, msg *any.Any) (*BlahResponse, error) {
	// var bWriter bytes.Buffer
	// writer := bufio.NewWriter(&bWriter)
	//
	// err := m.Marshal(writer, msg)
	// if err != nil {
	// 	return nil, err
	// }
	// var data []byte
	// bWriter.Read(data)
	// xml, err := mxj.AnyXmlIndent(data, "", "", "customer")
	// if err != nil {
	// 	return nil, err
	// }
	//
	// fmt.Println(string(xml))
	//
	// return &BlahResponse{Data: "Done"}, nil
	return something(msg)
}
func (b blahtter) EchoBlahStruct(c context.Context, msg *_struct.Struct) (*BlahResponse, error) {
	// var bWriter bytes.Buffer
	// writer := bufio.NewWriter(&bWriter)
	//
	// err := m.Marshal(writer, msg)
	// if err != nil {
	// 	return nil, err
	// }
	// var data []byte
	// bWriter.Read(data)
	// xml, err := mxj.AnyXmlIndent(data, "", "", "customer")
	// if err != nil {
	// 	return nil, err
	// }
	//
	// fmt.Println(string(xml))
	//
	// return &BlahResponse{Data: "Done"}, nil
	return something(msg)
}

func something(msg proto.Message) (*BlahResponse, error) {
	var bWriter bytes.Buffer
	writer := bufio.NewWriter(&bWriter)

	err := m.Marshal(writer, msg)
	if err != nil {
		return nil, err
	}
	writer.Flush()

	var data []byte
	bWriter.Read(data)
	xml, err := mxj.AnyXmlIndent(data, "", "", "customer")
	if err != nil {
		return nil, err
	}

	fmt.Println(string(xml))

	return &BlahResponse{Data: "Done"}, nil
}
